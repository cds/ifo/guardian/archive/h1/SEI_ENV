"""Node used to determine the environment conditions and then display
that as the state.

Should be one main decorator that will check and decide what state we 
should go into.

No edges should be necessary, all just jumpin.

**Update: 2019-12-13**: Lets start out by just doing EQs. 
States:
  CALM
    Nominal/start state
  SEISMON_ALERT
    If we didn't advance in 10min after event, jump back.
  EARTHQUAKE
    We currently have ground motion, should look at peakmon to get here
"""
from guardian import GuardState, GuardStateDecorator, NodeManager
import gpstime
import cdsutils
from timeout_utils import call_with_timeout
import math
from ISC_library import unstall_nodes
# Careful with edges that are defined in construction!
from construction import *

#request = 'IDLE'
nominal = 'AUTOMATIC'
nodes = NodeManager(['SEI_CONF', 'SEI_DIFF'])

#############################################
# Decorator and supporting functions/constants

useism_thresh = 800

peakmon_thresh = 400
peakmon_high = 600
peakmon_large = 6000

y3_raw = lambda j: 0.75 * j + 0


def myround(x, base=60):
    """Used to round to the nearest minute to allow for
    use with minute trends.
    """
    return int(base * round(float(x)/base))

def seismon_active():
    """Return True/False if there is an active seismon alert.
    By active I mean, in at least the green band.
    """
    for i in range(5):
        if math.log(ezca['SEI-SEISMON_EQ_LHO_DISTANCE_M_{}'.format(i)] / 1, 10)\
           < y3_raw(ezca['SEI-SEISMON_EQ_MAGNITUDE_MMS_{}'.format(i)]) and \
           ezca['SEI-SEISMON_LHO_R35_ARRIVALTIME_TOTAL_SECS_{}'.format(i)] > -300:
            return True
    # If we made it this far, theres no active seismon
    return False


def peakmon_active(thresh=peakmon_thresh):
    """See if there is some current ground motion on peakmon."""
    if ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] > thresh:
        return True
    else:
        return False


def picket_fence_active(thresh=peakmon_thresh):
     """See if there is some current ground motion on peakmon."""
     if ezca['SEI-USGS_NETWORK_PEAK'] > thresh:
         return True
     else:
         return False
    
def eq_check(gmotion='CALM'):
    class eq_check_dec(GuardStateDecorator):
        """Look for any seismon, peakmon, or picket fence.
        Move appropriately

        Add a kwarg for where it should go, ie. useism_eq or seismon_active_useism, etc
        """
        def pre_exec(self):
            if seismon_active():
                log('Seismon active')
                if gmotion == 'CALM':
                    return 'SEISMON_ALERT'
                else:
                    return 'SEISMON_ALERT_USEISM'
            elif peakmon_active(thresh=peakmon_high):
                log(f"Peakmon high ({int(ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'])}), no seismon alert, jumping to earthquake state")
                if gmotion == 'CALM':
                    return 'EARTHQUAKE'
                else:
                    return 'USEISM_EARTHQUAKE'
            """elif picket_fence_active(thresh=peakmon_high):                ## Commented out 06/15/2024 so as to not have picket fence trigger earthquake mode
                log(f"Picket fence network peak high ({int(ezca['SEI-USGS_NETWORK_PEAK'])}), no seismon alert, jumping to earthquake state")
                if gmotion == 'CALM':
                    return 'EARTHQUAKE'
                else:
                    return 'USEISM_EARTHQUAKE'"""

    return eq_check_dec

class check_env_state(GuardStateDecorator):
    """Main decorator

    """
    def pre_exec(self):
        return env_check()





def env_check(cur_state, lookback=7200):
    """
    """

    # This will need a timer so we dont do it every loop
    useism_data = call_with_timeout(cdsutils.getdata,
                                    ['H1:ISI-GND_STS_ITMY_Z_BLRMS_100M_300M.mean,m-trend',
                                     'H1:ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON.mean,m-trend'],
                                    lookback,
                                    start=myround(int(gpstime.gpsnow() - lookback)))
    # Case for no data returned
    if not useism_data:
        # Sleep for 30 sec then try again
        time.sleep(30)
        useism_data = call_with_timeout(cdsutils.getdata,
                                        ['H1:ISI-GND_STS_ITMY_Z_BLRMS_100M_300M.mean,m-trend',
                                         'H1:ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON.mean,m-trend'],
                                        lookback,
                                        start=myround(int(gpstime.gpsnow() - lookback)))
        if not useism_data:
            # Still no data, give up for now
            return True

    blrms_data = useism_data[0].data
    peakmon_data = useism_data[1].data
    peakmon_data_ignore = 1000
    # If peakmon is high we don't care about the peakmon vals
    valid_useism = []
    for i, val in enumerate(peakmon_data):
        if val < peakmon_data_ignore:
            valid_useism.append(blrms_data[i])
    if max(valid_useism) > useism_thresh:
        if cur_state != 'USEISM':
            return 'USEISM'
        else:
            return True
    elif max(valid_useism) < useism_thresh and cur_state != 'CALM':
        return 'CALM'
    else:
        return True

# Idea for use with a liner fit slope to determine if it should change state
# IF we did this we would have to get fancy with how it takes out the peakmon vals
# In [27]: linregress(range(useism_data_now[0].data.size), useism_data_now[0].data)
#Out[27]: LinregressResult(slope=2.638374966168012, intercept=414.2592110376062, rvalue=0.5321988203718,
# pvalue=1.2062867167969241e-05, stderr=0.5511081471375642, intercept_stderr=18.852141579833237)


#############################################
# States

# Old idle state generator, keeping if we need for future
def gen_idle_state(index, statename):
    class IDLE(GuardState):
        def run(self):
            state = env_check('')
            if state != statename:
                return state
            else:
                return True
    IDLE.index = index
    return IDLE


class INIT(GuardState):
    request = True

    def main(self):
         log('Initializing subordinate nodes:')
         nodes.set_managed()

         return 'AUTOMATIC'


class AUTOMATIC(GuardState):
    """The default state for this node. It should mainly be used
    as the request state and should decide what state to jump to.


    We could use this as a way to run better checks after maintenance
    to understand where to go since we might not be able to trust our
    seismometer data during that time.

    """
    index = 5
    request = True
    goto = False
    def main(self):
        # FIXME: Do a more in depth env check for when we go out of maintenance or similar
        return env_check('')


class IDLE(GuardState):
    """An idle state to go to when we need to test or put
    the rest of the config in non standard configs.
    """
    index = 3
    goto = True
    request = True
    def run(self):
        return True


class CALM(GuardState):
    index = 10
    goto = False
    request = False
    def main(self):
        nodes['SEI_DIFF'] = 'BSC2_FULL_DIFF_CPS'
        nodes['SEI_CONF'] = 'WINDY'
        self.timer['check_env'] = 0
        # To help avoid frequent changes back and forth
        self.timer['first_check'] = 3600

    @unstall_nodes(nodes)
    @eq_check()
    def run(self):
        if self.timer['first_check']:
            if self.timer['check_env']:
                self.timer['check_env'] = 1800
                return env_check('CALM')
            else:
                return True
        else:
            return True


class USEISM(GuardState):
    index = 15
    goto = False
    request = False
    def main(self):
        nodes['SEI_DIFF'] = 'BSC2_FULL_DIFF_CPS'
        nodes['SEI_CONF'] = 'USEISM'
        self.timer['check_env'] = 0
        # To avoid frequent changes back and forth
        self.timer['first_check'] = 3600

    @unstall_nodes(nodes)
    @eq_check(gmotion='USEISM')
    def run(self):
        if self.timer['first_check']:
            if self.timer['check_env']:
                self.timer['check_env'] = 1800
                return env_check('USEISM')
            else:
                return True
        else:
            return True


class MAINTENANCE(GuardState):
    index = 100
    goto = True
    request = True
    def main(self):
        nodes['SEI_DIFF'] = 'DOWN'
        nodes['SEI_CONF'] = 'SC_OFF_NOBRSXY'
        return
    def run(self):
        notify('Select AUTOMATIC, then INIT to get out of maintenance')
        return True
'''
class LIGHT_MAINTENANCE(GuardState):
    index = 111
    goto = True
    request = True
    def main(self):
        nodes['SEI_DIFF'] = 'DOWN'
        nodes['SEI_CONF'] = 'NOBRSXY_WINDY'
        return
    def run(self):
        if peakmon_active(thresh=peakmon_large):
            # ground motion large go to large eq sc off state
            return 'LARGE_EQ'
        notify('Select AUTOMATIC, then INIT to get out of maintenance')
        return True
'''
class LIGHT_MAINTENANCE_WINDY(GuardState):
    index = 111
    goto = True
    request = True
    def main(self):
        nodes['SEI_DIFF'] = 'DOWN'
        nodes['SEI_CONF'] = 'NOBRSXY_WINDY'
        return
    def run(self):
        if peakmon_active(thresh=peakmon_large):
            # ground motion large go to large eq sc off state
            return 'LARGE_EQ'
        notify('Select AUTOMATIC, then INIT to get out of maintenance')
        return True
        
class LIGHT_MAINTENANCE_USEISM(GuardState):
    index = 112
    goto = True
    request = True
    def main(self):
        nodes['SEI_DIFF'] = 'DOWN'
        nodes['SEI_CONF'] = 'NOBRSXY_USEISM'
        return
    def run(self):
        if peakmon_active(thresh=peakmon_large):
            # ground motion large go to large eq sc off state
            return 'LARGE_EQ'
        notify('Select AUTOMATIC, then INIT to get out of maintenance')
        return True        
        
#############
def gen_seismon_alert(index, return_state='CALM'):
    class SEISMON_ALERT_STATE(GuardState):
        """Wait for EQ. After seismon alert expires, wait another
        10min before going back.
        """
        request = False
        def main(self):
            self.timer['wait_for_eq'] = 0
            self.seismon_expired = False

        @unstall_nodes(nodes)
        def run(self):
            if peakmon_active():
                log('Peakmon active, moving to earthquake state')
                if return_state == 'CALM':
                    return 'EARTHQUAKE'
                else:
                    return 'USEISM_EARTHQUAKE'
            elif picket_fence_active():
                log('Picket fence active with seismon alert, moving to earthquake state')
                if return_state == 'CALM':
                    return 'EARTHQUAKE'
                else:
                    return 'USEISM_EARTHQUAKE'
            elif not seismon_active() and not self.seismon_expired:
                self.seismon_expired = True
                log('Seismon expired, waiting for 10min.')
                self.timer['wait_for_eq'] = 600
            elif not seismon_active() and self.seismon_expired and self.timer['wait_for_eq']:
                log('EQ didnt show')
                return return_state
    SEISMON_ALERT_STATE.index = index
    return SEISMON_ALERT_STATE

SEISMON_ALERT = gen_seismon_alert(20)
SEISMON_ALERT_USEISM = gen_seismon_alert(25, return_state='USEISM')


def gen_earthquake(nodes, index, return_state='CALM'):
    class EARTHQUAKE_STATE(GuardState):

        def main(self):
            if return_state == 'CALM':
                nodes['SEI_CONF'] = 'EARTH_QUAKE'
            else:
                nodes['SEI_CONF'] = 'USEISM_EARTHQUAKE'
            nodes['SEI_DIFF'] = 'BSC2_FULL_DIFF_CPS'
            self.timer['calm'] = 0
            self.calm = False

        @unstall_nodes(nodes)
        def run(self):
            if not peakmon_active(thresh=peakmon_high) and not self.calm and self.timer['calm']:
                self.calm = True
                # 10min? Sure why not
                self.timer['calm'] = 600
            elif peakmon_active(thresh=peakmon_large):
                # ground motion large go to large eq sc off state
                return 'LARGE_EQ'
            elif peakmon_active(thresh=peakmon_high) and self.calm:
                # reset the calm
                self.calm = False
            elif not peakmon_active(thresh=peakmon_high) and self.calm and self.timer['calm']:
                log(f'Low ground for 10min, going back to {return_state}')
                return return_state
    EARTHQUAKE_STATE.index = index
    return EARTHQUAKE_STATE

EARTHQUAKE = gen_earthquake(nodes, 30)
EARTHQUAKE.request = False
USEISM_EARTHQUAKE = gen_earthquake(nodes, 35, return_state='USEISM')
USEISM_EARTHQUAKE.request = False


class LARGE_EQ(GuardState):
    # FIXME: How long are we staying in this state? When can we move out?
    # If we move out too soon, we risk having another wave hit us.
    index = 40
    def main(self):
        nodes['SEI_CONF'] = 'LARGE_EQ_NOBRSXY'
        nodes['SEI_DIFF'] = 'DOWN'
        self.timer['calm'] = 0
        self.calm = False
        
    @unstall_nodes(nodes)
    def run(self):
        if not peakmon_active(thresh=peakmon_large) and not self.calm and self.timer['calm']:
            self.calm = True
            # 10min? Sure why not
            self.timer['calm'] = 900
        elif peakmon_active(thresh=peakmon_large) and self.calm:
            # reset the calm
            self.calm = False
        elif not peakmon_active(thresh=peakmon_large) and self.calm and self.timer['calm']:
            log('Low ground for 15min, going back to EARTHQUAKE')
            return 'EARTHQUAKE'


# Careful with edges since they are currently defined in construction!!!
