import time
from guardian import GuardState, NodeManager

bypasshours_in_seconds = 72.0 * 3600.0

swwdProtectedSUS = {'HAM2': ['MC1','MC3','PRM','PR3'],
                    'HAM3': ['MC2','PR2'],
                    'HAM4': ['SR2'],
                    'HAM5': ['SR3','SRM'],
                    'HAM6': ['OMC'],
                    'BS': ['BS'],
                    'ITMY': ['ITMY'],
                    'ITMX': ['ITMX'],
                    'ETMX': ['ETMX', 'TMSX'],
                    'ETMY': ['ETMY', 'TMSY']}

# HAM1 not listed, because HAM1 is special.
allChambers = ['HAM2','HAM3','HAM4','HAM5','HAM6','BS','ITMY','ITMX','ETMX','ETMY']
allHAMs = allChambers[0:5]
allBSCs = allChambers[5:]

allSus = ['ETMY',
          'ETMX',
          'ITMY',
          'ITMX',
          'BS',
          'MC1',
          'MC2',
          'MC3',
          'PRM',
          'PR2',
          'PR3',
          'SRM',
          'SR2',
          'SR3',
          'TMSY',
          'TMSX',
          'OFI',
          #'OPO',
          'OMC',
          'OM1',
          'OM2',
          'OM3',
          'RM1',
          'RM2',
          'IM1',
          'IM2',
          'IM3',
          'IM4',
          #'ZM1',
          'ZM2'
          ]

allManagers = ['SEI_{}'.format(chamber) for chamber in allChambers]
allManagers.append('HPI_HAM1')

manager_nodes = NodeManager(allManagers)
sus_nodes = NodeManager(['SUS_{}'.format(sus) for sus in allSus])
imc_node = NodeManager(['IMC_LOCK'])

class CONSTRUCTION(GuardState):
    """ See discussion in M2100056 """
    index = 200
    goto = True
    redirect = False
    request = False
    def main(self):
        # Unmanage the IMC and set to OFFLINE
        imc_node['IMC_LOCK'].release()
        imc_node['IMC_LOCK'] = 'OFFLINE'

        # Turn off CPS DIFF
        nodes['SEI_DIFF'] = 'DOWN'

        # Turn off site-wide sensor correction
        nodes['SEI_CONF'] = 'SC_OFF_NOBRSXY'

        # Unmanaged SEI_BS
        manager_nodes['SEI_BS'].release()

        # Initialize counter for sequncing in next method
        self.counter = 0

    def run(self):

        # We need to first wait for the IMC to go to OFFLINE,
        # then requestSEI managers move and wait to complete,
        # then the rest
        if imc_node.arrived and self.counter == 0:
            # IF we write a sitewide feedback loop manager,
            # then this can become a single request of that node.
            # For now "do it by hand."
            # Also, don't *need* to turn off end-stations for
            # A+ filter cavity road construction, but may be good
            # for future construction. Up for debate.
            for thisChamber in allChambers:
                manager_nodes['SEI_{}'.format(thisChamber)] = 'DAMPED'
            # HAM1 is special.
            manager_nodes['HPI_HAM1'] = 'READY'
            self.counter = 1

        elif manager_nodes.arrived and self.counter == 1:
            # Switch all intertial sensors to low gain
            geophoneDOFs = ['H1', 'H2', 'H3', 'V1', 'V2', 'V3']  # geophones = L4Cs and GS13s
            t240DOFs = ['X1', 'X2', 'X3']  # X DOF FM10 controls gain switch for all DOFs

            for thisChamber in allHAMs:
                if any(L4Cs in thisChamber for L4Cs in ['4', '5']):
                    for thisL4CDOF in geophoneDOFs:
                        ezca.get_LIGOFilter('ISI-{}_L4CINF_{}'.format(thisChamber, thisL4CDOF)).turn_on('FM4')
                for thisGS13DOF in geophoneDOFs:
                    ezca.get_LIGOFilter('ISI-{}_GS13INF_{}'.format(thisChamber, thisGS13DOF)).turn_on('FM4')
                    ezca.get_LIGOFilter('ISI-{}_GS13INF_{}'.format(thisChamber, thisGS13DOF)).turn_off('FM5')

            for thisChamber in allBSCs:
                for thisT240DOF in t240DOFs:
                    ezca.get_LIGOFilter('ISI-{}_ST1_T240INF_{}'.format(thisChamber, thisT240DOF)).turn_off('FM10')
                for thisL4CDOF in geophoneDOFs:
                    ezca.get_LIGOFilter('ISI-{}_ST1_L4CINF_{}'.format(thisChamber, thisL4CDOF)).turn_on('FM4')
                for thisGS13DOF in geophoneDOFs:
                    ezca.get_LIGOFilter('ISI-{}_ST2_GS13INF_{}'.format(thisChamber, thisGS13DOF)).turn_on('FM4')
                    ezca.get_LIGOFilter('ISI-{}_ST2_GS13INF_{}'.format(thisChamber, thisGS13DOF)).turn_off('FM5')

            # Increase allowable count of actuator saturations to max
            maxActSatCount = 8192
            for thisChamber in allHAMs:
                ezca['ISI-{}_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = maxActSatCount
            for thisChamber in allBSCs:
                ezca['ISI-{}_ST1_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = maxActSatCount
                time.sleep(0.1)
                ezca['ISI-{}_ST2_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = maxActSatCount
                time.sleep(0.1)

            ### SUS STUFF

            # Switch all SUS to ROBUST DAMPED
            for node in allSus:
                sus_nodes['SUS_{}'.format(node)] = 'ROBUST_DAMPED'

            # Increase bypass time on IOP SWWDs
            for thisChamber in allChambers:
                for thisOptic in swwdProtectedSUS[thisChamber]:
                    ezca['IOP-SUS_{}_DACKILL_BPTIME'.format(thisOptic)] = bypasshours_in_seconds

            # Press the bypass button.
            for thisChamber in allChambers:
                for thisOptic in swwdProtectedSUS[thisChamber]:
                    ezca['IOP-SUS_{}_DACKILL_BPSET'.format(thisOptic)] = 1

            # Turn off BS OpLev damping
            for yp in ['Y', 'P']:
                ezca.get_LIGOFilter('SUS-BS_M2_OLDAMP_{}'.format(yp)).turn_off('OUTPUT')

            self.counter = 2

        elif sus_nodes.arrived and self.counter == 2:
            # YAY we made it!!!
            log('Construction state complete')
            return True

        else:
            if self.counter == 0:
                notify('Waiting for IMC_LOCK')
            elif self.counter == 1:
                notify('Waiting for SEI managers to complete')
            elif self.counter == 2:
                notify('Waiting for SUS nodes to complete')
            else:
                notify('Waiting, but not sure why...')


class REVERTING_CONSTRUCTION(GuardState):
    """Revert the changes made in the last state that other states wont cover

    """
    index = 210
    redirect = False
    goto = False
    request = False

    def main(self):
        # Switch all intertial sensors back to high gain
        geophoneDOFs = ['H1', 'H2', 'H3', 'V1', 'V2', 'V3']  # geophones = L4Cs and GS13s
        t240DOFs = ['X1', 'X2', 'X3']  # X DOF FM10 controls gain switch for all DOFs

        for thisChamber in allHAMs:
            if any(L4Cs in thisChamber for L4Cs in ['4', '5']):
                for thisL4CDOF in geophoneDOFs:
                    ezca.get_LIGOFilter('ISI-{}_L4CINF_{}'.format(thisChamber, thisL4CDOF)).turn_off('FM4')
            if thisChamber == 'HAM6':
                continue
            else:
                for thisGS13DOF in geophoneDOFs:
                    ezca.get_LIGOFilter('ISI-{}_GS13INF_{}'.format(thisChamber, thisGS13DOF)).turn_on('FM4', 'FM5')

        for thisChamber in allBSCs:
            for thisT240DOF in t240DOFs:
                ezca.get_LIGOFilter('ISI-{}_ST1_T240INF_{}'.format(thisChamber, thisT240DOF)).turn_on('FM10')
            for thisL4CDOF in geophoneDOFs:
                ezca.get_LIGOFilter('ISI-{}_ST1_L4CINF_{}'.format(thisChamber, thisL4CDOF)).turn_off('FM4')
            for thisGS13DOF in geophoneDOFs:
                ezca.get_LIGOFilter('ISI-{}_ST2_GS13INF_{}'.format(thisChamber, thisGS13DOF)).turn_on('FM4', 'FM5')

        # IF we write a sitewide feedback loop manager,
        # then this can become a single request of that node.
        # For now "do it by hand."
        # Also, don't *need* to turn off end-stations for
        # A+ filter cavity road construction, but may be good
        # for future construction. Up for debate.
        for ham in allHAMs:
            manager_nodes['SEI_{}'.format(ham)] = 'ISOLATED'
        for bsc in allBSCs:
            if bsc == 'BS':
                manager_nodes['SEI_{}'.format(bsc)] = 'FULLY_ISOLATED_NO_ST2_BOOST'
            else:
                manager_nodes['SEI_{}'.format(bsc)] = 'FULLY_ISOLATED'
        # HAM1 is special.
        manager_nodes['HPI_HAM1'] = 'ROBUST_ISOLATED'

        # Return allowable count of actuator saturations to max
        nomActSatCount = 2048
        for thisChamber in allHAMs:
            # HAM6 is special
            if thisChamber == 'HAM6':
                continue
            else:
                ezca['ISI-{}_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = nomActSatCount
        for thisChamber in allBSCs:
            ezca['ISI-{}_ST1_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = nomActSatCount
            ezca['ISI-{}_ST2_WD_ACT_SET_SAFE_COUNT'.format(thisChamber)] = nomActSatCount

        ### SUS STUFF

        # Switch all SUS out of ROBUST_DAMPED, ALIGNED since not all sus are managed
        # IMC_LOCK is still in OFFLINE so we dont have to worry about flashes
        for node in allSus:
            if node == 'MC2':
                sus_nodes['SUS_{}'.format(node)] = 'MISALIGNED'
            else:
                sus_nodes['SUS_{}'.format(node)] = 'ALIGNED'

        # Return bypass time on IOP SWWDs
        for thisChamber in allChambers:
            for thisOptic in swwdProtectedSUS[thisChamber]:
                ezca['IOP-SUS_{}_DACKILL_BPTIME'.format(thisOptic)] = 1200

        # Press the reset button.
        for thisChamber in allChambers:
            for thisOptic in swwdProtectedSUS[thisChamber]:
                ezca['IOP-SUS_{}_DACKILL_RESET'.format(thisOptic)] = 1

        # Turn o BS OpLev damping
        for yp in ['Y', 'P']:
            ezca.get_LIGOFilter('SUS-BS_M2_OLDAMP_{}'.format(yp)).turn_on('OUTPUT')

    # Temporarily removed this dec
    #@nodes.checker()
    @manager_nodes.checker()
    @sus_nodes.checker()
    @imc_node.checker()
    def run(self):
        # Check that all nodes have arrived before returning True
        if manager_nodes.arrived and sus_nodes.arrived:
            return True


class CONSTRUCTION_REVERTED(GuardState):
    """Idle state to confirm that the changes have been brought back

    """
    index = 220
    goto = False
    request = False

    def run(self):
        notify('Select INIT to return to normal operation')
        return True


edges = [('CONSTRUCTION', 'REVERTING_CONSTRUCTION'),
         ('REVERTING_CONSTRUCTION', 'CONSTRUCTION_REVERTED')
         ]
